package it.com.example.beersupply.admin;

import com.atlassian.jira.pageobjects.JiraTestedProduct;
import com.atlassian.pageobjects.TestedProductFactory;
import com.atlassian.pageobjects.page.HomePage;
import com.atlassian.pageobjects.page.LoginPage;

import com.example.beersupply.pageobjects.BeerSupplyAdminPage;
import com.example.beersupply.pageobjects.BeerSupplyAdminSetup;

import org.junit.Test;

/**
 * @since version
 */
public class AdminSetupTest
{
    private JiraTestedProduct product = TestedProductFactory.create(JiraTestedProduct.class);

    @Test
    public void setupIsSuccessful() throws Exception
    {
        HomePage home = product.visit(LoginPage.class).loginAsSysAdmin(HomePage.class);

        BeerSupplyAdminPage beerSupplyAdminPage = product.visit(BeerSupplyAdminPage.class);
        BeerSupplyAdminSetup adminSetup = beerSupplyAdminPage.beginSetup();
        
        assert adminSetup.getSuccess();
    }
}
