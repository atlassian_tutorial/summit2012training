package it.com.example.beersupply.webpanel;

import javax.ws.rs.core.MediaType;

import com.atlassian.jira.pageobjects.JiraTestedProduct;
import com.atlassian.pageobjects.TestedProductFactory;
import com.atlassian.pageobjects.page.HomePage;
import com.atlassian.pageobjects.page.LoginPage;

import com.example.beersupply.pageobjects.BeerSupplyAdminPage;
import com.example.beersupply.pageobjects.BeerSupplyAdminSetup;
import com.example.beersupply.pageobjects.ViewBeerIssuePage;
import com.example.beersupply.util.BasicAuthHandler;
import com.example.beersupply.util.IssueUtil;

import org.apache.wink.client.ClientConfig;
import org.apache.wink.client.Resource;
import org.apache.wink.client.RestClient;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertTrue;

/**
 * @since version
 */
public class BeerRatingWebPanelTest
{
    public static String baseUrl = System.getProperty("baseurl") + "/rest/beersupply/1.0/beer";
    private static JiraTestedProduct product = TestedProductFactory.create(JiraTestedProduct.class);

    @BeforeClass
    public static void setupIsSuccessful() throws Exception
    {
        HomePage home = product.visit(LoginPage.class).loginAsSysAdmin(HomePage.class);

        BeerSupplyAdminPage beerSupplyAdminPage = product.visit(BeerSupplyAdminPage.class);
        BeerSupplyAdminSetup adminSetup = beerSupplyAdminPage.beginSetup();

        assert adminSetup.getSuccess();
    }

    @Test
    public void helloSummitFound() throws Exception
    {
        String issueKey = IssueUtil.createBeerIssue("hello beer");

        ViewBeerIssuePage viewIssuePage = product.visit(ViewBeerIssuePage.class,issueKey);
        
        assertTrue(viewIssuePage.hasHelloMessage());
    }

    @Test
    public void radioRatingWorks() throws Exception
    {
        String issueKey = IssueUtil.createBeerIssue("radio beer");

        ViewBeerIssuePage viewIssuePage = product.visit(ViewBeerIssuePage.class,issueKey);
        viewIssuePage.clickRadio3();

        String userRatingUrl = baseUrl + "/" + issueKey + "/rating/user";
        Resource userRatingResource = getRestResource(userRatingUrl);

        String userRating = userRatingResource.get(String.class);

        assertEquals("3.0",userRating);

    }

    @Ignore
    @Test
    public void ratyRatingWorks() throws Exception
    {
        String issueKey = IssueUtil.createBeerIssue("raty beer");

        ViewBeerIssuePage viewIssuePage = product.visit(ViewBeerIssuePage.class,issueKey);
        viewIssuePage.clickRaty2();
    
        String userRatingUrl = baseUrl + "/" + issueKey + "/rating/user";
        Resource userRatingResource = getRestResource(userRatingUrl);

        String userRating = userRatingResource.get(String.class);
        

        assertEquals("2.0",userRating);

    }

    private Resource getRestResource(String url)
    {
        BasicAuthHandler authHandler = new BasicAuthHandler("admin","admin");

        ClientConfig config = new ClientConfig();
        config.handlers(authHandler);

        config.proxyHost("localhost");
        config.proxyPort(8888);

        RestClient restClient = new RestClient(config);
        Resource resource = restClient.resource(url);

        resource.contentType(MediaType.APPLICATION_JSON_TYPE);
        resource.accept(MediaType.APPLICATION_JSON_TYPE);

        return resource;
    }
}
