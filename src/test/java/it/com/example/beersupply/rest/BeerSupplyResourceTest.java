package it.com.example.beersupply.rest;

import java.net.URI;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriBuilder;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.pageobjects.JiraTestedProduct;
import com.atlassian.pageobjects.TestedProductFactory;
import com.atlassian.pageobjects.page.HomePage;
import com.atlassian.pageobjects.page.LoginPage;

import com.example.beersupply.pageobjects.BeerSupplyAdminPage;
import com.example.beersupply.pageobjects.BeerSupplyAdminSetup;

import org.apache.wink.client.ClientConfig;
import org.apache.wink.client.Resource;
import org.apache.wink.client.RestClient;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.example.beersupply.util.BasicAuthHandler;
import com.example.beersupply.util.IssueUtil;

import static junit.framework.Assert.assertEquals;


/**
 * @since version
 */
public class BeerSupplyResourceTest
{
    private static JiraTestedProduct product = TestedProductFactory.create(JiraTestedProduct.class);
    private String baseUrl = System.getProperty("baseurl") + "/rest/beersupply/1.0/beer";
                                                            

    @BeforeClass
    public static void setupIsSuccessful() throws Exception
    {
        HomePage home = product.visit(LoginPage.class).loginAsSysAdmin(HomePage.class);

        BeerSupplyAdminPage beerSupplyAdminPage = product.visit(BeerSupplyAdminPage.class);
        BeerSupplyAdminSetup adminSetup = beerSupplyAdminPage.beginSetup();

        assert adminSetup.getSuccess();
    }

    @Test
    public void singleUserRatingEqualsAverage() throws Exception
    {
        String issueKey = IssueUtil.createBeerIssue("beer number one");

        String resourceUrl = baseUrl + "/" + issueKey + "/rate";

        Resource resource = getRestResource(resourceUrl);
        
       String myRating = "3.0";
        
        String average = resource.post(String.class,myRating);
        
        assertEquals(myRating,average);
    }

    @Test
    public void getUserRatingWorks() throws Exception
    {
        String issueKey = IssueUtil.createBeerIssue("beer number two");

        String resourceUrl = baseUrl + "/" + issueKey + "/rate";

        Resource rateResource = getRestResource(resourceUrl);
        String myRating = "2.0";
        String average = rateResource.post(String.class,myRating);


        String userRatingUrl = baseUrl + "/" + issueKey + "/rating/user";
        Resource userRatingResource = getRestResource(userRatingUrl);
        
        String userRating = userRatingResource.get(String.class);
        
        assertEquals(myRating,userRating);
    }

    @Test
    public void getAverageRatingWorks() throws Exception
    {
        String issueKey = IssueUtil.createBeerIssue("beer number three");

        String resourceUrl = baseUrl + "/" + issueKey + "/rate";

        Resource rateResource = getRestResource(resourceUrl);
        String myRating = "4.0";
        String returnAverage = rateResource.post(String.class,myRating);


        String averageRatingUrl = baseUrl + "/" + issueKey + "/rating/average";
        Resource averageRatingResource = getRestResource(averageRatingUrl);

        String average = averageRatingResource.get(String.class);

        assertEquals(myRating,average);
    }
    
    private Resource getRestResource(String url)
    {
        BasicAuthHandler authHandler = new BasicAuthHandler("admin","admin");

        ClientConfig config = new ClientConfig();
        config.handlers(authHandler);

        config.proxyHost("localhost");
        config.proxyPort(8888);

        RestClient restClient = new RestClient(config);
        Resource resource = restClient.resource(url);

        resource.contentType(MediaType.APPLICATION_JSON_TYPE);
        resource.accept(MediaType.APPLICATION_JSON_TYPE);
        
        return resource;
    }
}
