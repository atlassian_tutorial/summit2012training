package com.example.beersupply.util;

import org.apache.wink.client.ClientAuthenticationException;
import org.apache.wink.client.ClientRequest;
import org.apache.wink.client.ClientResponse;
import org.apache.wink.client.handlers.AbstractAuthSecurityHandler;
import org.apache.wink.client.handlers.ClientHandler;
import org.apache.wink.client.handlers.HandlerContext;
import org.apache.wink.common.http.HttpStatus;
import org.apache.wink.common.internal.i18n.Messages;


public class BasicAuthHandler extends AbstractAuthSecurityHandler implements ClientHandler
{

    private static final int UNAUTHORIZED = HttpStatus.UNAUTHORIZED.getCode();

    public BasicAuthHandler()
    {
        /* do nothing */
    }

    public BasicAuthHandler(final String username, final String password)
    {
        super(username, password);
    }

    public ClientResponse handle(ClientRequest request, HandlerContext context) throws Exception
    {
        if (handlerEncodedCredentials == null)
        {
            handlerEncodedCredentials = getEncodedString(handlerUsername, handlerPassword);
        }

        request.getHeaders().putSingle("Authorization", handlerEncodedCredentials);
        ClientResponse response = context.doChain(request);

        if (response.getStatusCode() == UNAUTHORIZED)
        {
            throw new ClientAuthenticationException(Messages.getMessage("serviceFailedToAuthenticateUser", handlerUsername));
        }
        else
        {
            return response;
        }
    }
}