package com.example.beersupply.ao;

import com.atlassian.activeobjects.test.TestActiveObjects;
import com.google.common.collect.Iterables;
import net.java.ao.EntityManager;
import net.java.ao.test.converters.NameConverters;
import net.java.ao.test.jdbc.Data;
import net.java.ao.test.jdbc.DatabaseUpdater;
import net.java.ao.test.jdbc.DerbyEmbedded;
import net.java.ao.test.jdbc.Jdbc;
import net.java.ao.test.junit.ActiveObjectsJUnitRunner;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertTrue;

@RunWith(ActiveObjectsJUnitRunner.class)
@Data(BeerRatingServiceImplTest.BeerRatingServiceImplTestDatabaseUpdater.class)
@Jdbc(DerbyEmbedded.class)
@NameConverters
public class BeerRatingServiceImplTest {

    private EntityManager entityManager;
    private BeerRatingServiceImpl service;

    @Before
    public void setup() {
        TestActiveObjects testAO = new TestActiveObjects(entityManager);
        service = new BeerRatingServiceImpl(testAO);
    }

    @Test
    public void addCreatesRow() throws Exception {
        assertEquals(0, entityManager.find(BeerRating.class).length);
        service.add("BEER-1", "user", 5.0d);
        BeerRating[] ratings = entityManager.find(BeerRating.class);
        assertEquals(1, ratings.length);
        assertEquals(ratings[0].getIssueKey(), "BEER-1");
        assertEquals(ratings[0].getUsername(), "user");
        assertEquals(ratings[0].getRating(), 5.0d);
    }

    @Test
    public void deleteRemovesRow() throws Exception {
        String issueKey = "BEER-2";
        String username = "joey";
        service.add(issueKey, username, 5.0d);
        BeerRating[] ratings = entityManager.find(BeerRating.class);
        assertEquals(1, ratings.length);
        service.delete(issueKey, username);
        ratings = entityManager.find(BeerRating.class);
        assertEquals(0, ratings.length);
    }

    @Test
    public void getVotingUsers() throws Exception {
        String issueKey = "BEER-3";
        service.add(issueKey, "fred", 5.0d);
        service.add(issueKey, "dave", 3.0d);
        service.add("BEER-4", "fred", 4.0d);
        BeerRating[] ratings = entityManager.find(BeerRating.class);
        assertEquals(3, ratings.length);
        Iterable<String> usernames = service.getVotingUsers();

        assertEquals(Iterables.size(usernames), 2);
        assertTrue(Iterables.contains(usernames, "fred"));
        assertTrue(Iterables.contains(usernames, "dave"));
    }

    public static final class BeerRatingServiceImplTestDatabaseUpdater implements DatabaseUpdater {
        @Override
        public void update(EntityManager entityManager) throws Exception {
            entityManager.migrate(BeerRating.class);
        }
    }

}
