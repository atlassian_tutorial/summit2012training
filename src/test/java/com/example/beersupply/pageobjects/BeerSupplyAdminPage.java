package com.example.beersupply.pageobjects;

import javax.inject.Inject;

import com.atlassian.pageobjects.Page;
import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.binder.WaitUntil;
import com.atlassian.webdriver.AtlassianWebDriver;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * @since version
 */
public class BeerSupplyAdminPage implements Page
{
    public static final String BREWERY_DB_API_KEY = "b937d4a045cc3aeb1358a2d7196c68e6";
    @Inject
    private PageBinder pageBinder;

    @Inject
    private AtlassianWebDriver webDriver;

    @FindBy(id = "beersupplyAdminButton")
    private WebElement button;

    @FindBy(id = "apiKey")
    private WebElement apiKeyField;


    @Override
    public String getUrl()
    {
        return "/plugins/servlet/admin/beersupplyadmin";
    }

    @WaitUntil
    public void waitUntilBody()
    {
        webDriver.waitUntilElementIsLocated(By.id("beersupply-container"));

    }

    public BeerSupplyAdminSetup beginSetup() throws Exception
    {
        apiKeyField.sendKeys(BREWERY_DB_API_KEY);
        button.click();
        
        return pageBinder.bind(BeerSupplyAdminSetup.class);
    }


}
