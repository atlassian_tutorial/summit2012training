AJS.namespace("beerview");
(function ()
{
    // Declare a local copy of beerview to instantiate on the window object.
    // access with the global "beerview" variable.
    var localBeerView = function ()
    {

        var baseImagePath;
        var currentIssueKey;
        this.setupCarousel = function (firstBeerId, path)
        {
            baseImagePath = path;

            jQuery(".averageRating").each(function (index)
            {
                var issueKey = jQuery(this).attr("id").split("_")[1];
                var rating = jQuery(this).attr("rating");

                jQuery(this).raty({
                    readOnly:true,
                    start:rating,
                    hintList:['swill', 'yellow fiz', 'drinkable', 'tasty', 'amazing'],
                    noRatedMsg:'not rated',
                    path:baseImagePath,
                    starOn:'beer-icon.png',
                    starOff:'beer-icon-grey.png',
                    starHalf:'beer-icon-half.png',
                    target:"#averageHint_" + issueKey,
                    targetKeep:true
                });
            });

            jQuery(".userRating").each(function (index)
            {
                var issueKey = jQuery(this).attr("id").split("_")[1];
                var rating = jQuery(this).attr("rating");

                jQuery(this).raty({
                    start:rating,
                    hintList:['swill', 'yellow fiz', 'drinkable', 'tasty', 'amazing'],
                    noRatedMsg:'not rated',
                    path:baseImagePath,
                    starOn:'beer-icon.png',
                    starOff:'beer-icon-grey.png',
                    starHalf:'beer-icon-half.png',
                    target:"#userHint_" + issueKey,
                    targetKeep:true,
                    click:function (score, evt)
                    {
                        jQuery.ajax(AJS.contextPath() + "/rest/beersupply/1.0/beer/" + issueKey + "/rate",
                        {
                                'data':score,
                                'type':'POST',
                                'processData':false,
                                'dataType':'json',
                                'contentType':'application/json',
                                'success':function (data)
                                {
                                    jQuery('#average_' + issueKey).raty('readOnly', false);
                                    jQuery('#average_' + issueKey).raty('start', data);
                                    jQuery('#average_' + issueKey).raty('readOnly', true);
                                }
                        });
                    }
                });
            });

            jQuery(".commentButton").click(function(event){
                var issueKey = jQuery(this).attr("issueKey");
                var body = jQuery("#commentField_" + issueKey).val();
                jQuery.ajax(AJS.contextPath() + "/rest/api/2/issue/" + issueKey + "/comment",
                    {
                        'data':'{"body":"' + body + '"}',
                        'type':'POST',
                        'processData':false,
                        'dataType':'json',
                        'contentType':'application/json',
                        'success':function (data)
                        {
                            jQuery("#commentField_" + issueKey).val("");
                            beerview.loadComments(issueKey);
                        }
                    });
            });

            jQuery('#featured').orbit({
                animation:'fade',
                captions:true,
                pauseOnHover:true,
                afterSlideChange:function (prev, active)
                {
                    beerview.updateBeerDetails(active, prev);
                },
                afterLoadComplete:function (active)
                {
                    var orbit = this;
                    beerview.updateBeerDetails(jQuery(firstBeerId), null);

                    jQuery(".beerDetails").mouseenter(function(){
                        orbit.stopClock();
                    });
                }

            });
        };

        this.updateBeerDetails = function (active, prev)
        {
            var issueKey = active.attr("issue-key");
            if (prev != null && prev != undefined)
            {
                var OLDissueKey = prev.attr("issue-key");
                jQuery("#comments_" + OLDissueKey).empty();
                jQuery("#commentdetails_" + OLDissueKey).fadeOut('fast');
                jQuery("#details_" + OLDissueKey).fadeOut('fast', function()
                {
                    beerview.showDetails(issueKey);
                });

            }
            else
            {
                this.showDetails(issueKey);
            }
        };

        this.showDetails = function(issueKey)
        {
            jQuery("#details_" + issueKey).fadeIn();
            jQuery("#commentdetails_" + issueKey).fadeIn();

            this.loadComments(issueKey);
        };

        this.loadComments = function(issueKey)
        {
            jQuery.getJSON(AJS.contextPath() + "/rest/api/2/issue/" + issueKey + "/comment",function(data){
                var comments = data.comments;
                if(comments.length > 0)
                {
                    jQuery("#comments_" + issueKey).empty();
                    jQuery("#comments_" + issueKey).append('<div id="commentSlider_' + issueKey + '"></div>');
                    jQuery.each(comments, function(i,comment){
                        jQuery("#commentSlider_" + issueKey).append(
                            '<div class="comment"><a href="' + AJS.contextPath() + '/jira/secure/ViewProfile.jspa?name=' + comment.author.name + '">' + comment.author.displayName + '</a><br/> ' + comment.body + '</div>'
                        );
                    });

                    jQuery("#commentSlider_" + issueKey).orbit({
                        animation:'vertical-slide',
                        directionalNav:false,
                        captions:false
                    });
                }

            });
        };


    };

    // Globally register beerview. We are using 'new' so that the 'this' references in beerview
    // correctly point to the beerview object, not the window.
    window.beerview = new localBeerView();

})();
