package com.example.beersupply.ao;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import com.atlassian.activeobjects.external.ActiveObjects;

import com.google.common.collect.ImmutableList;

import net.java.ao.DBParam;
import net.java.ao.EntityStreamCallback;
import net.java.ao.Query;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * @since version
 */
public class BeerStyleServiceImpl implements BeerStyleService
{
    /**
     * The ActiveObjects component will be injected
     */
    private final ActiveObjects ao;

    public BeerStyleServiceImpl(ActiveObjects ao)
    {
        this.ao = checkNotNull(ao);
    }

    @Override
    public BeerStyle add(String styleName)
    {
        return ao.create(BeerStyle.class,new DBParam(DBPARAM_STYLE_NAME,styleName));
    }

    @Override
    public List<BeerStyle> getAll()
    {
        final List<BeerStyle> styles = new ArrayList<BeerStyle>();

        ao.stream(BeerStyle.class,new EntityStreamCallback<BeerStyle, Integer>() {
            @Override
            public void onRowRead(BeerStyle beerStyle)
            {
                styles.add(beerStyle);
            }
        });

        return ImmutableList.copyOf(styles);
    }

    @Override
    public BeerStyle getByName(String name)
    {
        BeerStyle[] styles = ao.find(BeerStyle.class, Query.select().where("style_name = ?",name).limit(1));

        if(null != styles && styles.length > 0)
        {
            return styles[0];
        }

        return null;
    }

    @Override
    public BeerStyle getById(Integer id)
    {
        return ao.get(BeerStyle.class,id);
    }

    @Override
    public int getCount()
    {
        return ao.count(BeerStyle.class);
    }
}
