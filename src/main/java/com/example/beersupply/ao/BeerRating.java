package com.example.beersupply.ao;

import net.java.ao.Entity;
import net.java.ao.Preload;
import net.java.ao.schema.Indexed;
import net.java.ao.schema.NotNull;

/**
 * @since version
 */
@Preload
public interface BeerRating extends Entity
{
    @NotNull //must have a value
    public String getIssueKey();
    public void setIssueKey(String key);

    @NotNull //must have a value
    public String getUsername();
    public void setUsername(String name);

    @NotNull //must have a value
    public Double getRating();
    public void setRating(Double rating);
}
