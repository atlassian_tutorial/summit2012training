package com.example.beersupply.ao;

import java.util.List;

import com.example.beersupply.ao.BeerRating;

/**
 * @since version
 */
public interface BeerRatingService
{
    /**
     * The database parameter name for the BeerStyle styleName.
     * This is essentially the name of the field, all uppercase with _ separating camel-casing
     */
    public static final String DBPARAM_ISSUE_KEY = "ISSUE_KEY";
    public static final String DBPARAM_USERNAME = "USERNAME";
    public static final String DBPARAM_RATING = "RATING";

    BeerRating add(String issueKey, String userName, Double rating);

    void delete(String issueKey, String userName);

    Iterable<String> getVotingUsers();

    List<BeerRating> getRatingsForIssue(String issueKey);

    BeerRating getRatingForIssueByUser(String issueKey, String username);

    BeerRating getById(Integer id);

    int getCount();

    int getCountByIssue(String issueKey);

    Double getSumByIssue(String issueKey);

}
