package com.example.beersupply.brewerydb;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import com.atlassian.sal.api.net.*;

import com.example.beersupply.brewerydb.representations.BeerDescriptor;
import com.example.beersupply.brewerydb.representations.BeersCollection;
import com.example.beersupply.brewerydb.representations.Style;
import com.example.beersupply.brewerydb.representations.StylesCollection;
import com.google.gson.Gson;

import org.apache.commons.io.IOUtils;

/**
 * @since version
 */
public class BreweryDBClientImpl implements BreweryDBClient
{
    private final RequestFactory<?> requestFactory;

    public BreweryDBClientImpl(RequestFactory<?> requestFactory)
    {
        this.requestFactory = requestFactory;
    }

    @Override
    public List<Style> getAllBeerStyles(String apiKey)
    {
        final List<Style> styleList = new ArrayList<Style>();

        String fullUrl = BEER_STYLES_URL + "?key=" + apiKey;

        Request request = requestFactory.createRequest(Request.MethodType.GET, fullUrl);
        try
        {
            request.execute(new ResponseHandler()
            {
                @Override
                public void handle(Response response) throws ResponseException
                {
                    try
                    {
                        String stylesJson = IOUtils.toString(response.getResponseBodyAsStream());
                        Gson gson = new Gson();
                        StylesCollection styles = gson.fromJson(stylesJson, StylesCollection.class);

                        styleList.addAll(styles.getData());
                    }
                    catch (IOException e)
                    {
                        throw new ResponseException(e);
                    }
                }
            });
        }
        catch (ResponseException e)
        {

        }
        return styleList;
    }

    @Override
    public List<BeerDescriptor> search(String searchTerm, String apiKey)
    {
        final List<BeerDescriptor> beerList = new ArrayList<BeerDescriptor>();


        String fullUrl = null;
        try
        {
            fullUrl = BEER_SEARCH_URL + "?key=" + apiKey + "&q=" + URLEncoder.encode(searchTerm, "UTF-8") + "&type=beer&withBreweries=Y";
        }
        catch (UnsupportedEncodingException e)
        {
            e.printStackTrace();
            
            return beerList;
        }

        Request request = requestFactory.createRequest(Request.MethodType.GET, fullUrl);

        try
        {
            request.execute(new ResponseHandler()
            {
                @Override
                public void handle(Response response) throws ResponseException
                {
                    try
                    {
                        String beersJson = IOUtils.toString(response.getResponseBodyAsStream());
                        Gson gson = new Gson();
                        BeersCollection beers = gson.fromJson(beersJson, BeersCollection.class);

                        beerList.addAll(beers.getData());
                    }
                    catch (IOException e)
                    {
                        throw new ResponseException(e);
                    }

                }
            });
        }
        catch (ResponseException e)
        {

        }

        return beerList;
    }

}
