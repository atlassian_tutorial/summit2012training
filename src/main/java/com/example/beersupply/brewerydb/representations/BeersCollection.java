package com.example.beersupply.brewerydb.representations;

import java.util.List;

/**
 * @since version
 */
public class BeersCollection
{
    private List<BeerDescriptor> data;

    public List<BeerDescriptor> getData()
    {
        return data;
    }
}
