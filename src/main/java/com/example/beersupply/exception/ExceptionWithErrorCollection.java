package com.example.beersupply.exception;

import com.atlassian.jira.util.ErrorCollection;

/**
 * @since version
 */
public class ExceptionWithErrorCollection extends Exception
{
    private ErrorCollection errorCollection;

    public ExceptionWithErrorCollection(ErrorCollection errors)
    {
        this.errorCollection = errors;
    }

    public ExceptionWithErrorCollection(String message,ErrorCollection errors)
    {
        super(message);
        this.errorCollection = errors;
    }

    public ExceptionWithErrorCollection(String message, ErrorCollection errors, Throwable cause)
    {
        super(message, cause);
        this.errorCollection = errors;
    }

    public ExceptionWithErrorCollection(ErrorCollection errors, Throwable cause)
    {
        super(cause);
        this.errorCollection = errors;
    }

    public ErrorCollection getErrorCollection()
    {
        return errorCollection;
    }
}
