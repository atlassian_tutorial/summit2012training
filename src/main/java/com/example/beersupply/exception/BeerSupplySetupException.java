package com.example.beersupply.exception;

import com.atlassian.jira.util.ErrorCollection;

/**
 * @since version
 */
public class BeerSupplySetupException extends ExceptionWithErrorCollection
{
    public BeerSupplySetupException(ErrorCollection errors)
    {
        super(errors);
    }

    public BeerSupplySetupException(String message, ErrorCollection errors)
    {
        super(message, errors);
    }

    public BeerSupplySetupException(String message, ErrorCollection errors, Throwable cause)
    {
        super(message, errors, cause);
    }

    public BeerSupplySetupException(ErrorCollection errors, Throwable cause)
    {
        super(errors, cause);
    }
}
