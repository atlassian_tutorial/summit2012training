package com.example.beersupply.jira.customfields;

import com.atlassian.jira.issue.customfields.converters.DoubleConverter;
import com.atlassian.jira.issue.customfields.impl.NumberCFType;
import com.atlassian.jira.issue.customfields.manager.GenericConfigManager;
import com.atlassian.jira.issue.customfields.persistence.CustomFieldValuePersister;

/**
 * @since version
 */
public class BeerAverageRatingCFType extends NumberCFType
{
    public static final String CF_KEY = "com.example.beersupply.atlassian-beer-supply:beer-supply-average-rating-cf";
    public static final String SEARCHER_KEY = "com.example.beersupply.atlassian-beer-supply:beer-average-rating-searcher";

    public BeerAverageRatingCFType(CustomFieldValuePersister customFieldValuePersister, DoubleConverter doubleConverter, GenericConfigManager genericConfigManager)
    {
        super(customFieldValuePersister, doubleConverter, genericConfigManager);
    }
}
