package com.example.beersupply.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * An interface for defining injectable components that can handle servlet requests
 */
public interface ServletHandler
{
    /**
     * The method called by the servlet to handle the request
     * @param request
     * @param response
     * @throws IOException
     * @throws ServletException
     */
    void handle(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException;
}
