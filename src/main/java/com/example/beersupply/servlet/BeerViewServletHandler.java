package com.example.beersupply.servlet;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.util.ErrorCollection;
import com.atlassian.jira.util.SimpleErrorCollection;
import com.atlassian.jira.util.velocity.VelocityRequestContextFactory;
import com.atlassian.plugin.webresource.UrlMode;
import com.atlassian.plugin.webresource.WebResourceUrlProvider;
import com.atlassian.templaterenderer.TemplateRenderer;

import com.example.beersupply.components.IssueToBeerConverter;
import com.example.beersupply.exception.ExceptionWithErrorCollection;

import org.apache.commons.lang.StringUtils;

/**
 * @since version
 */
public class BeerViewServletHandler implements ServletHandler
{
    private final VelocityRequestContextFactory requestContextFactory;
    private final TemplateRenderer renderer;
    private final WebResourceUrlProvider webResourceUrlProvider;
    private final IssueToBeerConverter issueToBeerConverter;
    private final JiraAuthenticationContext authContext;
    private final LoginEnforcer loginEnforcer;

    public BeerViewServletHandler(VelocityRequestContextFactory requestContextFactory, TemplateRenderer renderer, WebResourceUrlProvider webResourceUrlProvider, IssueToBeerConverter issueToBeerConverter, JiraAuthenticationContext authContext, LoginEnforcer loginEnforcer)
    {
        this.requestContextFactory = requestContextFactory;
        this.renderer = renderer;
        this.webResourceUrlProvider = webResourceUrlProvider;
        this.issueToBeerConverter = issueToBeerConverter;
        this.authContext = authContext;
        this.loginEnforcer = loginEnforcer;
    }

    @Override
    public void handle(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException
    {
        if(null == authContext.getLoggedInUser())
        {
            loginEnforcer.redirectToLogin(request, response);
            return;
        }

        //set our content type
        response.setContentType("text/html;charset=utf-8");

        //create our velocity context
        final Map<String, Object> context = new HashMap<String, Object>();

        context.put("requestContext", requestContextFactory.getJiraVelocityRequestContext());

        //get our path->result template mapping
        ServletMapping mapping = ServletMapping.fromPath(request);

        try
        {
            switch (mapping)
            {
                case VIEW_BEERS:
                    handleViewBeers(request, response, context, mapping.getResultTemplate());
                    break;
                case NOT_MAPPED:
                    handleViewBeers(request, response, context, mapping.getResultTemplate());
                    break;
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
            ErrorCollection errorCollection;
            if(e instanceof ExceptionWithErrorCollection)
            {
                errorCollection = ((ExceptionWithErrorCollection)e).getErrorCollection();
            }
            else
            {
                String error = (e.getMessage() != null)? e.getMessage() : e.getClass().getName();
                String message = "Error: " + error;
                errorCollection = new SimpleErrorCollection();
                errorCollection.addErrorMessage(message);
            }

            String errorTemplate = ServletMapping.ERROR.getResultTemplate();
            context.put("errorCollection", errorCollection);

            renderer.render(errorTemplate, context, response.getWriter());
        }
    }

    private void handleViewBeers(HttpServletRequest request, HttpServletResponse response, Map<String, Object> context, String resultTemplate) throws Exception
    {
        String imagePath = webResourceUrlProvider.getStaticPluginResourceUrl("com.example.beersupply.atlassian-beer-supply:beer-supply-resources", "images", UrlMode.ABSOLUTE);

        context.put("baseImagePath", StringUtils.chomp(imagePath,"/"));
        context.put("beers",issueToBeerConverter.getBeersFromIssues(authContext.getLoggedInUser()));

        renderer.render(resultTemplate, context, response.getWriter());
    }
}
